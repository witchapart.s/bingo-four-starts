import express from 'express'
import bodyParser from 'body-parser'
import dotenv from 'dotenv'
import { Client } from 'pg'
import cors from 'cors'

import { getAllLobby, createRoom, searchroom } from './lobby'
import { getRoominfo, playroomUpdate, playroomSit1, playroomSit2, playroomDisconnect } from './playroom'

dotenv.config()
const client = new Client({
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  database: process.env.DB_NAME,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD
})

client.connect()
const app = express()
app.use(bodyParser.json())
app.use(cors())

const createDropButtonArr = (columnDisplay: number) => {
  var createDropButton: any = []
  for (var i = 1; i <= columnDisplay; i++) {
    createDropButton.push({ column: i, row: 1 })
  }
  return createDropButton
}

const createTableArr = (rowDisplay: number, columnDisplay: number) => {
  var createTable: any = []
  for (var j = rowDisplay; j > 0; j--) {
    for (var i = 1; i <= columnDisplay; i++) {
      createTable.push({ column: i, row: j, symbol: '' })
    }
  }
  return createTable
}

app.get('/lobby', async (req: any, res: any) => { getAllLobby(req, res, client) })

app.post('/lobby/createroom/', async (req: any, res: any) => {
  console.log('req.body.',req.body);
  
  createRoom(req, res, client, createDropButtonArr(req.body.column), createTableArr(req.body.row, req.body.column))
})

app.get('/lobby/searchroom/:roomInfo', async (req: any, res: any) => { searchroom(req, res, client) })

app.get('/playroom/:roomId', async (req: any, res: any) => { getRoominfo(req, res, client) })

app.put('/playroom/update/', async (req: any, res: any) => { playroomUpdate(req, res, client) })

app.put('/playroom/sit1/', async (req: any, res: any) => { playroomSit1(req, res, client) })

app.put('/playroom/sit2/', async (req: any, res: any) => { playroomSit2(req, res, client) })

// app.put('/playroom/disconnect/', async (req: any, res: any) => { playroomDisconnect(req, res, client) })

app.get('/playroom/disconnect/:socketId', async (req: any, res: any) => {
  client.query(`select * from playroom WHERE player1_socket_id = '${req.params.socketId}' or player2_socket_id = '${req.params.socketId}'`,
    async (err: any, result: any) => {
      if (err) {
        console.error('Error executing query 64', err)
      } else {
        res.json(result.rows[0])
      }
    })
})

app.delete('/playroom/delete/:roomId', async (req: any, res: any) => {
  client.query(`DELETE FROM playroom WHERE room_id='${req.params.roomId}'`,async (err: any, result: any) => {
      if (err) {
        console.error('Error executing query 74', err)
      }
    })
})

app.put(`/lobby/restartRoom/`, async (req: any, res: any) => { 
  client.query(`UPDATE playroom SET 
  active_box='{}',
  drop_button='${JSON.stringify(createDropButtonArr(req.body.data.column))}',
  display_table='${JSON.stringify(createTableArr(req.body.data.row,req.body.data.column))}'
  WHERE room_id=${req.body.data.roomId}`,async (err: any, result: any) => {
    if (err) {
      console.error('Error executing query 74', err)
    } else {
      client.query(`SELECT * FROM playroom WHERE room_id = '${req.body.data.roomId}'`, async (err: any, result: any) => {
        if (err) {
            console.error('115 => Error executing query', err)
        } else {
            res.json(result.rows[0])
        }
    })
    }
  })
})


app.get('/', (req: any, res: any) => {
  res.send(`<h1>Backend server running on port ${process.env.HOST_PORT}</h1>`)
})

app.listen(process.env.HOST_PORT, () => {
  console.log(`[server]: Server is running at http://localhost:${process.env.HOST_PORT}`)
})
