export const getRoominfo = (req: any, res: any, client: any) => {
    client.query(`SELECT * FROM playroom WHERE room_id = '${req.params.roomId}'`, async (err: any, result: any) => {
        if (err) {
            console.error('Error executing query', err, 'getroom 4')
        } else {
            res.json(result.rows[0])
        }
    })
}

export const playroomUpdate = (req: any, res: any, client: any) => {
    client.query(`
        UPDATE playroom SET 
        room_name='${req.body.roomState.room_name}',
        player1_socket_id='${req.body.roomState.player1_socket_id}', 
        player1_name='${req.body.roomState.player1_name}', 
        player2_socket_id='${req.body.roomState.player2_socket_id}', 
        player2_name='${req.body.roomState.player2_name}', 
        player_turn='${req.body.roomState.player_turn}',
        active_box='${JSON.stringify(req.body.roomState.active_box)}',
        drop_button='${JSON.stringify(req.body.roomState.drop_button)}',
        display_table='${JSON.stringify(req.body.roomState.display_table)}'
        WHERE room_id=${req.body.roomState.room_id}
    `, async (err: any, result: any) => {
        if (err) {
            console.error('111 => Error executing query', err)
        } else {
            client.query(`SELECT * FROM playroom WHERE room_id = '${req.body.roomState.room_id}'`, async (err: any, result: any) => {
                if (err) {
                    console.error('115 => Error executing query', err)
                } else {
                    res.json(result.rows[0])
                }
            })
        }
    })
}

export const playroomSit1 = (req: any, res: any, client: any) => {
    client.query(`UPDATE playroom SET player1_socket_id='${req.query.player1_socket_id}', player1_name='${req.query.player1_name}', player_turn='${req.query.player1_name}' WHERE room_id=${req.query.room_id}`,
        async (err: any, result: any) => {
            if (err) {
                console.error('Error executing query', err)
            }
        })
}

export const playroomSit2 = (req: any, res: any, client: any) => {
    client.query(`UPDATE playroom SET player2_socket_id='${req.query.player2_socket_id}', player2_name='${req.query.player2_name}' WHERE room_id=${req.query.room_id}`,
        async (err: any, result: any) => {
            if (err) {
                console.error('Error executing query', err)
            }
        })
}
export const playroomDisconnect = (req: any, res: any, client: any) => {
    client.query(`
        UPDATE playroom
        SET player1_socket_id = NULL, player1_name = NULL
        WHERE player1_socket_id = '${req.query.socketId}';
        UPDATE playroom
        SET player2_socket_id = NULL, player2_name = NULL
        WHERE player2_socket_id = '${req.query.socketId}';`,
        async (err: any, result: any) => {
            if (err) {
                console.error('Error executing query', err)
            } else {
                console.log(result, '62 result');
            }
        })
}

