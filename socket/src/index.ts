import axios from 'axios'
import { Server } from 'socket.io'
import http from 'http'
import express from 'express'
import cors from 'cors'
import dotenv from 'dotenv'
const app = express()
app.use(cors())
dotenv.config()

const server = http.createServer(app)
const io = new Server(server, {
    cors: {
        origin: `${process.env.FRONTEND_HOST}/`,
        methods: ["GET", "POST"],
    },
})

const responseToOneClient = (clientId: string, eventName: string, dataArr: [] | {}) => {
    io.to(clientId).emit(eventName, dataArr)
}

const getRoomState = async (roomId: number) => {
    await axios.get(`${process.env.BACKEND_HOST}/playroom/${roomId}`).then((response: any) => {
        responseToOneClient(response.data.player1_socket_id, 'resJoinRoom', response.data)
        responseToOneClient(response.data.player2_socket_id, 'resJoinRoom', response.data)
    })
}

io.on("connection", (socket: any) => {
    console.log('connection : ', socket.id)

    socket.on('disconnect', () => {
        console.log('disconnect', socket.id)
        axios.get(`${process.env.BACKEND_HOST}/playroom/disconnect/${socket.id}`).then((response: any) => {
            axios.delete(`${process.env.BACKEND_HOST}/playroom/delete/${response.data.room_id}`)
            io.to(response.data.player1_socket_id).emit('roomFull')
            io.to(response.data.player2_socket_id).emit('roomFull')
        })
    })

    socket.on('createRoom', (data: any) => {
        console.log('createRoom',data);
        
        const { roomName, column, row } = data
        axios.post(`${process.env.BACKEND_HOST}/lobby/createroom/`, { roomName, column, row })
            .then((res) => {
                io.to(socket.id).emit('resCreateRoom', { roomId: res.data.room_id })
            })

    })

    socket.on('joinRoom', (data: any) => {
        const { roomInfo, displayName } = data
        axios.get(`${process.env.BACKEND_HOST}/playroom/${roomInfo}`).then(async (response: any) => {
            const roomDetail = response.data
            if (roomDetail.player1_socket_id === null) {
                axios.put(`${process.env.BACKEND_HOST}/playroom/sit1/?room_id=${roomDetail.room_id}&player1_socket_id=${socket.id}&player1_name=${displayName}`)
                getRoomState(roomInfo)
            } else if (roomDetail.player2_socket_id === null) {
                axios.put(`${process.env.BACKEND_HOST}/playroom/sit2/?room_id=${roomDetail.room_id}&player2_socket_id=${socket.id}&player2_name=${displayName}`)
                getRoomState(roomInfo)
            } else {
                io.to(socket.id).emit('roomFull')
            }
        })
    })

    socket.on("getAllLobby", () => {
        axios.get(`${process.env.BACKEND_HOST}/lobby`).then(async (response: any) => {
            io.emit("resLobby", response.data)
        })
    })


    socket.on("clickDropButton", (data: any) => {
        const { roomState, column, row } = data
        roomState.drop_button = JSON.parse(roomState.drop_button).map((button: any) => {
            if (button.column === column && button.row < 8) {
                return {
                    column: button.column,
                    row: button.row + 1
                }
            } else {
                return button
            }
        })

        roomState.display_table = JSON.parse(roomState.display_table).map((table: any) => {
            if (table.column === column && table.row === row) {
                return {
                    column: table.column,
                    row: table.row,
                    symbol: roomState.player_turn
                }
            } else {
                return table
            }
        })

        roomState.active_box = { column: column, row: row }
        if (roomState.player_turn === roomState.player1_name) {
            roomState.player_turn = roomState.player2_name
        } else {
            roomState.player_turn = roomState.player1_name
        }
        axios.put(`${process.env.BACKEND_HOST}/playroom/update/`, { roomState }).then(async (response: any) => {
            responseToOneClient(
                response.data.player1_socket_id,
                "returnRoomState",
                response.data
            )
            responseToOneClient(
                response.data.player2_socket_id,
                "returnRoomState",
                response.data
            )
        })
    })


    socket.on("reStartGame", (data: any) => {
        console.log('reStartGame', data)
        axios.put(`${process.env.BACKEND_HOST}/lobby/restartRoom/`,{data}).then(async (response: any) => {
            responseToOneClient(
                response.data.player1_socket_id,
                "returnRoomState",
                response.data
            )
            responseToOneClient(
                response.data.player2_socket_id,
                "returnRoomState",
                response.data
            )
        })

        // roomState = roomState.map((room: any) => {
        //     if (room.roomId === data.room) {
        //         return {
        //             roomId: room.roomId,
        //             player1: room.player1,
        //             player2: room.player2,
        //             gameStatus: true,
        //             activeBox: {},
        //             playerTurn: room.playerTurn,
        //             dropButton: createDropButtonArr(),
        //             table: createTableArr()
        //         }
        //     } else {
        //         return room
        //     }
        // })
        // responseToOneClient(
        //     roomState.filter((room: any) => (room.roomId === data.room))[0].player1.socketId,
        //     "returnRoomState",
        //     roomState.filter((room: any) => (room.roomId === data.room))
        // )
        // responseToOneClient(
        //     roomState.filter((room: any) => (room.roomId === data.room))[0].player2.socketId,
        //     "returnRoomState",
        //     roomState.filter((room: any) => (room.roomId === data.room))
        // )

        // responseToOneClient(
        //     response.data.player1_socket_id,
        //     "returnRoomState",
        //     response.data
        // )
        // responseToOneClient(
        //     response.data.player2_socket_id,
        //     "returnRoomState",
        //     response.data
        // )
    })


})
app.get('/', (req: any, res: any) => {
    res.send(`<h1>Socket server running on port ${process.env.HOST_PORT}</h1>`)
})

server.listen(process.env.HOST_PORT, () => {
    console.log(`[server]: Server is running at http://localhost:${process.env.HOST_PORT}`)
})