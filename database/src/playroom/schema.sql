--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16 (Debian 11.16-1.pgdg90+1)
-- Dumped by pg_dump version 11.16 (Debian 11.16-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: playroom; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.playroom (
    room_id integer NOT NULL,
    room_name character varying(255),
    player1_socket_id character varying(255),
    player1_name character varying(255),
    player2_socket_id character varying(255),
    player2_name character varying(255),
    player_turn character varying(255),
    active_box character varying(255),
    drop_button text,
    display_table text
);


ALTER TABLE public.playroom OWNER TO admin;

--
-- Name: Playroom_room_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

ALTER TABLE public.playroom ALTER COLUMN room_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Playroom_room_id_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- Name: playroom Playroom_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.playroom
    ADD CONSTRAINT "Playroom_pkey" PRIMARY KEY (room_id);


--
-- PostgreSQL database dump complete
--

