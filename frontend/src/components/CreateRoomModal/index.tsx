import React, { useEffect, useState } from 'react'
const CreateRoomModal = (props: any) => {
    const { createNewRoomModal, setcreateNewRoomModal, createNewRoom } = props
    const [roomName, setroomName] = useState<string>('')
    const [column, setcolumn] = useState<number>(8)
    const [row, setrow] = useState<number>(6)

    const createAction = () => {
        console.log('10', roomName, column, row)
        createNewRoom(roomName, column, row)
        setcreateNewRoomModal(false)
    }

    return (
        <div style={{ display: createNewRoomModal === true ? 'block' : 'none' }} >
            <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                <div className="relative w-auto my-6 mx-auto max-w-3xl">
                    {/* content */}
                    <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                        {/* header */}
                        <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                            <h3 className="text-3xl font-semibold">
                                Create New Room.
                            </h3>
                            <button
                                className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                                onClick={() => setcreateNewRoomModal(false)}
                            >
                                <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                                    ×
                                </span>
                            </button>
                        </div>
                        {/* body */}
                        <div className="relative p-6 flex-auto">
                            <div className='block mb-2 text-sm font-medium text-gray-900 dark:text-white'>
                                <input type='text' value={roomName} onChange={(e) => { setroomName(e.target.value) }} className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500' placeholder='ค้นหาชื่อห้อง' />

                            </div>
                            <label htmlFor="selectColumn" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Columns</label>
                            <select id="selectColumn" value={column} onChange={(e) => { setcolumn(Number(e.target.value)) }} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <option value={8}>8</option>
                                <option value={10}>10</option>
                                <option value={12}>12</option>
                                <option value={14}>14</option>
                            </select>
                            <label htmlFor="selectRow" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Rows</label>
                            <select id="selectRow" value={row} onChange={(e) => { setrow(Number(e.target.value)) }} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <option value={6}>6</option>
                                <option value={7}>7</option>
                                <option value={8}>8</option>
                                <option value={9}>9</option>
                            </select>
                        </div>
                        {/* footer */}
                        <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                            <button
                                className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                type="button"
                                onClick={() => setcreateNewRoomModal(false)}
                            >
                                Close
                            </button>
                            <button
                                className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                type="button"
                                onClick={() => createAction()}
                            >
                                Save Changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </div>
    )
}
export default CreateRoomModal