import React from 'react'

export const Layout = ({children}:any) => {
  return (
    <div className='px-[120px] pt-[20px]'>
        <main>{children}</main>
    </div>
  )
}
