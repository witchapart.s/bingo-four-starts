import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setPlayerName } from '../../store/playerReducer'
import { useNavigate } from 'react-router-dom'
import { RootState } from '../../store'
import axios from 'axios'
import CreateRoomModal from '../../components/CreateRoomModal'
const LobbyDisplayPage = () => {

  // start import function from libary
  const dispatch = useDispatch()
  const navigate = useNavigate()
  // end import function from libary

  // start get value from store
  const socketService = useSelector((state: RootState) => state.socket.socket)
  const displayName = useSelector((state: RootState) => state.player.player.name)
  // end get value from store

  // start state
  const [socket, setSocket] = useState<any>(socketService)
  const [lobby, setlobby] = useState<{ room_id: number, room_name: string }[]>([])
  const [roomName, setroomName] = useState<string>('')
  const [createNewRoomModal, setcreateNewRoomModal] = useState(false)
  // end state

  // start one time run useEffect
  useEffect(() => {
    clearSearchRoomName()
  }, [])
  // end one time run useEffect

  // start socket useEffect
  useEffect(() => {
    if (socket !== null) {
      socket.on('resLobby', (data: any) => {
        setlobby(data)
      })
      socket.on('resCreateRoom', (data: any) => {
        socket.emit('getAllLobby')
        goToRoom(data.roomId)
      })
    }
  }, [socket])
  // end socket useEffect

  // start function
  const createNewRoom = (roomName: string, column: number, row: number) => {
    socket.emit('createRoom', { roomName, column, row })
  }
  const clearSearchRoomName = () => {
    setroomName('')
    axios.get(`${process.env.REACT_APP_BACKEND_HOST}/lobby`).then((response: any) => {
      setlobby(response.data)
    })
  }
  const searchRoomName = () => {
    if (roomName !== '') {
      axios.get(`${process.env.REACT_APP_BACKEND_HOST}/lobby/searchroom/${roomName}`).then((response: any) => {
        setlobby(response.data)
      })
    } else {
      clearSearchRoomName()
    }
  }
  const goToRoom = (roomId: number) => {
    navigate(`/playroom/${roomId}`)
  }
  // end function


  return (
    <>
      <CreateRoomModal
        key={'CreateRoomModal'}
        createNewRoomModal={createNewRoomModal}
        setcreateNewRoomModal={setcreateNewRoomModal}
        createNewRoom={createNewRoom}
      />
      <div className='flex bg-blue-300 px-[10px] py-[5px] justify-end mb-[20px]'>
        <div className='ml-[10px]'>
          <input type='text' value={displayName} onChange={(e) => { dispatch(setPlayerName(e.target.value)) }} className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500' placeholder='ชื่อผู้เล่น' required />
        </div>
        <div className='ml-[10px]'>
          <input type='text' value={roomName} onChange={(e) => { setroomName(e.target.value) }} className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500' placeholder='ค้นหาชื่อห้อง' required />
        </div>
        <div className='ml-[10px]'>
          <button onClick={() => { searchRoomName() }} className='bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded'>
            ค้นหาห้อง
          </button>
        </div>
        <div className='ml-[10px]'>
          <button onClick={() => { clearSearchRoomName() }} className='bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded'>
            ล้างการค้นหา
          </button>
        </div>
        <div className='ml-[10px]'>
          <button
            data-modal-target="defaultModal"
            data-modal-toggle="defaultModal"
            type="button"
            onClick={() => { setcreateNewRoomModal(!createNewRoomModal) }}
            className='bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded'>
            สร้างห้อง
          </button>
        </div>
      </div>
      <div className='roomContainer flex'>
        {
          lobby.map((room, index) => (
            <div key={index} className='m-[5px] border border-gray-200 hover:bg-gray-100 rounded-md mb-[10px]'>
              <h5 className='mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white'>เลขห้อง : {room.room_id}</h5>
              <p className='font-normal text-gray-700 dark:text-gray-400'>ชื่อห้อง : {room.room_name}</p>
              <button onClick={() => { goToRoom(room.room_id) }} className='w-[100%] bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded'>
                เช้าร่วม
              </button>
            </div>
          ))
        }
      </div>
    </>
  )
}
export default LobbyDisplayPage