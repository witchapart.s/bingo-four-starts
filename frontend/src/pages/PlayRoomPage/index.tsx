import React from 'react'
import { useState, useEffect } from 'react'
import { PlusCircleOutlined, MinusCircleOutlined } from '@ant-design/icons'
import { useLocation, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import { RootState } from '../../store'
const PlayRoomPage = () => {

    // start import function from libary
    const location = useLocation()
    const navigate = useNavigate()
    const dispatch = useDispatch()
    // end import function from libary

    // start get value from store
    const socketService = useSelector((state: RootState) => state.socket.socket)
    const displayName = useSelector((state: RootState) => state.player.player.name)
    // end get value from store

    // start state
    const [socket, setSocket] = useState<any>(socketService)
    const [roomState, setroomState] = useState<any>(null)
    const [winner, setwinner] = useState<any>(null)
    // end state
    const [roomFull, setroomFull] = useState(false)
    // start server state
    const [playerInroom, setplayerInroom] = useState<{ socketId: string, playerName: string }[]>([])
    const [statePlayerTurn, setstatePlayerTurn] = useState<string | null>(null)
    const [gameStatus, setgameStatus] = useState<boolean>(false)
    const [activeBox, setactiveBox] = useState<{ column: number, row: number } | any>({})
    const [dropButton, setdropButton] = useState<{ column: number, row: number }[]>([])
    const [tableDisplay, settableDisplay] = useState<{ column: number, row: number, symbol: string }[]>([])
    // end server state

    // start one time run useEffect
    useEffect(() => {
        socket.emit('joinRoom', { roomInfo: location.pathname.split('/')[2], displayName })
    }, [])
    // end one time run useEffect

    useEffect(() => {
        console.log('roomFull', roomFull);

    }, [roomFull])
    useEffect(() => {
        console.log('roomState', roomState);

    }, [roomState])

    // start dependency run useEffect
    useEffect(() => {
        if (roomState !== null) {
            setplayerInroom([
                { socketId: roomState?.player1_socket_id, playerName: roomState?.player1_name },
                { socketId: roomState?.player2_socket_id, playerName: roomState?.player2_name }
            ])
            setstatePlayerTurn(roomState.player_turn)
            setactiveBox(JSON.parse(roomState.active_box))
            setdropButton(JSON.parse(roomState.drop_button))
            settableDisplay(JSON.parse(roomState.display_table))
        }
    }, [roomState])
    useEffect(() => {
        if (playerInroom[0]?.playerName !== undefined && playerInroom[0]?.playerName !== null && playerInroom[1]?.playerName !== undefined && playerInroom[1]?.playerName !== null) {
            setgameStatus(true)
        }
    }, [playerInroom])
    // end dependency run useEffect

    // start socket useEffect
    useEffect(() => {
        if (socket !== null) {
            // when user join
            socket.on("resJoinRoom", (data: any) => {
                setroomState(data)
            })
            // when user join play
            socket.on("returnRoomState", (data: any) => {
                setroomState(data)
            })
            socket.on('roomFull', () => {
                setroomFull(true)
            })
        }
    }, [socket])
    // end socket useEffect




    const reStartGame = () => {
        socket.emit("reStartGame", { roomId: roomState.room_id, column: dropButton.length, row: tableDisplay.length / dropButton.length })
        setwinner(null)
    }

    const reportWinner = (player: any, scoreArr: any) => {
        if (Math.max(...scoreArr) >= 4) {
            setwinner(player)
            setgameStatus(false)
        }
    }

    const returnPoint = (column: any, row: any, player: any) => {
        if (column >= 1 && column <= 12 && row >= 1 && row <= 8) {
            if (tableDisplay.filter((box: any) => (box.symbol === player && box.column === column && box.row === row)).length !== 0) {
                return 1
            } else {
                return 0
            }
        }
        else {
            return 0
        }
    }

    useEffect(() => {
        if (activeBox !== undefined) {
            var symbol = tableDisplay.filter((box: any) => (box.column === activeBox.column && box.row === activeBox.row))[0]?.symbol
            var x_score = -1
            for (var i = activeBox.column; i <= 12; i++) {
                if (returnPoint(i, activeBox.row, symbol) === 0) { break }
                x_score += returnPoint(i, activeBox.row, symbol)
            }
            for (var i = activeBox.column; i >= 1; i--) {
                if (returnPoint(i, activeBox.row, symbol) === 0) { break }
                x_score += returnPoint(i, activeBox.row, symbol)
            }
            // console.log('x_score', x_score)
            var y_score = 0
            for (var j = activeBox.row; j >= 1; j--) {
                if (returnPoint(activeBox.column, j, symbol) === 0) { break }
                y_score += returnPoint(activeBox.column, j, symbol)
            }
            // console.log('y_score', y_score)
            var right_slope_score = -1
            var right_up_slope = 0
            for (var i = activeBox.row; i <= 8; i++) {
                if (returnPoint(activeBox.column + right_up_slope, i, symbol) === 0) { break }
                right_slope_score += returnPoint(activeBox.column + right_up_slope, i, symbol)
                right_up_slope += 1
            }
            var right_down_slope = 0
            for (var i = activeBox.row; i >= 1; i--) {
                if (returnPoint(activeBox.column - right_down_slope, i, symbol) === 0) { break }
                right_slope_score += returnPoint(activeBox.column - right_down_slope, i, symbol)
                right_down_slope += 1
            }
            // console.log('right_slope_score', right_slope_score)
            var left_slope_score = -1
            var left_up_slope = 0
            for (var i = activeBox.row; i <= 8; i++) {
                if (returnPoint(activeBox.column - left_up_slope, i, symbol) === 0) { break }
                left_slope_score += returnPoint(activeBox.column - left_up_slope, i, symbol)
                left_up_slope += 1
            }
            var left_down_slope = 0
            for (var i = activeBox.row; i >= 1; i--) {
                if (returnPoint(activeBox.column + left_down_slope, i, symbol) === 0) { break }
                left_slope_score += returnPoint(activeBox.column + left_down_slope, i, symbol)
                left_down_slope += 1
            }
            // console.log('left_slope_score', left_slope_score)
            reportWinner(symbol, [x_score, y_score, right_slope_score, left_slope_score])
        }


    }, [activeBox])

    const onClickDropButton = (column: any, row: any) => {
        console.log(column, row, statePlayerTurn)
        socket.emit("clickDropButton", { roomState, column, row })
    }

    return (
        <>
            <div style={{ display: roomFull === false ? 'none' : 'block' }}>
                <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                    <div className="relative w-auto my-6 mx-auto max-w-3xl">
                        {/* content */}
                        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                            {/* header */}
                            <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                <h3 className="text-3xl font-semibold">
                                    something wrong!
                                </h3>
                                <button
                                    className="p-1 ml-auto bg-transparent border-0 text-black opacity-10 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                                    onClick={() => { navigate(`/Lobby/`) }}
                                >
                                    x
                                </button>
                            </div>
                            {/* body */}
                            <div className="relative p-6 flex-auto">
                                can't join this room because this room is full or deleted
                            </div>
                            {/* footer */}
                            <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                <button
                                    className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                    type="button"
                                    onClick={() => { navigate(`/Lobby/`) }}
                                >
                                    go back!
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
            </div>






            <div
                style={{ display: roomFull === true ? 'none' : 'block' }}
            >
                <div className='flex flex-wrap h-[50px] w-full mt-[10px] mb-[15px] px-[100px]'>
                    <div className='w-2/12 p-1'>
                        <input
                            className='h-full bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
                            value={displayName}
                            readOnly
                        />
                    </div>
                    <div className='w-2/12 p-1'>
                        <input
                            className='h-full bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
                            value={`${roomState?.room_id} ${roomState?.room_name}`}
                            readOnly
                        />
                    </div>
                </div>


                <div className='flex flex-wrap w-full mt-[10px] mb-[15px] px-[100px]'>
                    {
                        playerInroom.map((player) => (
                            <div className='w-1/2 p-1'>
                                <button className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-full'
                                >{player?.playerName !== null ? player.playerName : 'sit!'}</button>
                            </div>
                        ))
                    }
                </div>
                <div className='flex flex-wrap w-full mt-[10px] mb-[15px] px-[100px]' >
                    <p style={{ display: gameStatus === true ? 'block' : 'none' }}>{statePlayerTurn}'s turn</p>
                </div>


                <div className='flex flex-wrap w-full mt-[10px] mb-[15px] px-[100px]'>
                    {
                        dropButton?.map((button: any) => (
                            <div key={button.column} className='p-1' style={{ width: `${100 / dropButton.length}%` }}>
                                <button className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-full'
                                    disabled={!gameStatus || !(displayName === statePlayerTurn)}
                                    onClick={() => { onClickDropButton(button.column, button.row) }}
                                >{button.column}</button>
                            </div>
                        ))
                    }
                </div>
                <div className='flex flex-wrap w-full px-[100px]'>
                    {
                        tableDisplay.map((table: any, index: any) => (
                            <div key={`${index}-row-${table.row}-column-${table.column}`} className='p-1' style={{ width: `${(100 * (tableDisplay.length / dropButton.length)) / tableDisplay.length}%` }}>
                                <div className='flex justify-center items-center h-[50px] border-2'>
                                    {table.symbol === playerInroom[0]?.playerName ? <PlusCircleOutlined className='text-[24px] text-rose-500' />
                                        : table.symbol === playerInroom[1]?.playerName ? <MinusCircleOutlined className='text-[24px] text-purple-600' />
                                            : ''}
                                </div>
                            </div>
                        ))
                    }
                </div>
                <div className='flex  justify-center'>
                    <h1 style={{ display: winner !== null ? 'block' : 'none' }}>winner is {winner}</h1>
                </div>
                <div className='w-full px-[100px] mt-[10px] mb-[15px]'>
                    <button className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-full' onClick={() => { reStartGame() }}>restart!</button>
                </div>
            </div>
        </>
    )
}
export default PlayRoomPage