import './App.css'
import React, { Suspense } from 'react'
import { Layout } from './components/Layout'

import {
  createBrowserRouter,
  RouterProvider,
  Navigate,
  Route,
  Link,
} from "react-router-dom"


const LobbyDisplayPage = React.lazy(() => import('./pages/LobbyDisplayPage'))
const PlayRoomPage = React.lazy(() => import('./pages/PlayRoomPage'))

const router = createBrowserRouter([
  {
    path: "/Lobby",
    element: (<LobbyDisplayPage />)
  },
  {
    path: "/playroom/:roomId",
    element: (<PlayRoomPage />)
  },
  {
    path: "*",
    element: (<Navigate to="/Lobby" replace />)
  },
])


function App() {
  return (
    <>
      <Layout>
        <Suspense fallback={<div>Loading...</div>}>
          <RouterProvider router={router} />
        </Suspense>
      </Layout>
    </>
  )
}

export default App