import { configureStore  } from '@reduxjs/toolkit'
import socketReducer from './socketReducer'
import playerReducer from './playerReducer'

export const store = configureStore({
  reducer: {
    socket: socketReducer,
    player: playerReducer
  },
})
export type RootState = ReturnType<typeof store.getState>