import { createSlice } from '@reduxjs/toolkit'
import io from "socket.io-client"
// import type { PayloadAction } from '@reduxjs/toolkit'

interface playerState {
    player: {
        name:string
    }
}

const initialState: playerState = {
    player: {
        name:''
    }
}

export const playerSlice = createSlice({
    name: 'player',
    initialState,
    reducers: {
        setPlayerName: (state, action) => {
            state.player = {name:action.payload}
        }
    },
})

export const { setPlayerName } = playerSlice.actions
export default playerSlice.reducer