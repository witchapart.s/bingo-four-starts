import { createSlice } from '@reduxjs/toolkit'
import io from "socket.io-client"
// import type { PayloadAction } from '@reduxjs/toolkit'

interface socketState {
  socket: {}
}

const initialState: socketState = {
  socket: io(`${process.env.REACT_APP_SOCKET_HOST}`, { transports: ['websocket'] })
}

export const socketSlice = createSlice({
  name: 'socket',
  initialState,
  reducers: {
    testSocket: (state) =>{
      state.socket = io(`${process.env.REACT_APP_SOCKET_HOST}`, { transports: ['websocket'] })
    }
  },
})

export const {testSocket} = socketSlice.actions
export default socketSlice.reducer